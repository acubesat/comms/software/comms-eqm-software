#include "main.h"
#include "FreeRTOS.h"
#include "list.h"
#include "task.h"
#include "CurrentSensorsTask.hpp"
#include "MCUTemperatureTask.hpp"
#include "UARTGatekeeperTask.hpp"
#include "TemperatureSensorsTask.hpp"
#include "TimeKeepingTask.hpp"
#include "TransceiverTask.hpp"
#include "SBandTask.hpp"
#include "DummyTask.hpp"

extern SPI_HandleTypeDef hspi4;
extern UART_HandleTypeDef huart4;
extern I2C_HandleTypeDef hi2c1;


template<class T>
static void vClassTask(void *pvParameters) {
    (static_cast<T *>(pvParameters))->execute();
}

void blinkyTask1(void * pvParameters){
    for(;;){
        HAL_GPIO_TogglePin(LED_PE14_GPIO_Port, LED_PE14_Pin);
        HAL_Delay(500);
    }
}

void blinkyTask2(void * pvParameters){
    for(;;){
        HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_0);
        HAL_Delay(300);
    }
}

namespace AT86RF215 {
    AT86RF215 transceiver = AT86RF215(&hspi4, AT86RF215Configuration());
}

extern "C" void main_cpp(){
    uartGatekeeperTask.emplace();
//    transceiverTask.emplace();
    sBandTask.emplace();
//    currentSensorsTask.emplace();
//    mcuTemperatureTask.emplace();
//    timeKeepingTask.emplace();
//    temperatureSensorsTask.emplace();
//    dummyTask.emplace();

    uartGatekeeperTask->createTask();
//    transceiverTask->createTask();
    sBandTask->createTask();
//    currentSensorsTask->createTask();
//    mcuTemperatureTask->createTask();
//    timeKeepingTask->createTask();
//    temperatureSensorsTask->createTask();
//    dummyTask->createTask();

    /**
     * Uncomment below and comment above for Led task visualization (for STM32H743)
     */
//    xTaskCreate(blinkyTask1, "blinkyTask 1", 1000, nullptr, tskIDLE_PRIORITY + 1, nullptr);
//    xTaskCreate(blinkyTask2, "blinkyTask 2", 1000, nllptr, tskIDLE_PRIORITY + 1, nullptr);
    vTaskStartScheduler();
    for(;;);
    return;
}

/**
 * @brief This function handles EXTI line1 interrupts.
*/
extern "C" void EXTI1_IRQHandler(void) {
//    DummyTask::interruptCount++;
    HAL_GPIO_EXTI_IRQHandler(RF_IRQ_Pin);
//    HAL_GPIO_TogglePin(LED_PE15_GPIO_Port, LED_PE15_Pin);
    TransceiverTask::transceiver.handle_irq();
}
