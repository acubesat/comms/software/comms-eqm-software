#include "DummyTask.hpp"

extern UART_HandleTypeDef huart4;


void DummyTask::execute() {
    while (true) {
//        HAL_GPIO_TogglePin(LED_PE14_GPIO_Port, LED_PE14_Pin);
        LOG_DEBUG << "Interrupt Count: " << interruptCount;
        vTaskDelay(pdMS_TO_TICKS(500));
    }
}