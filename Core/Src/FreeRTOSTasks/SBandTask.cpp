#include "SBandTask.hpp"

//AT86RF215::AT86RF215 SBandTask::transceiver =

static AT86RF215::AT86RF215 transceiver = AT86RF215::AT86RF215(&hspi4, AT86RF215::AT86RF215Configuration());

SBandTask::PacketType SBandTask::createRandomPacket(uint16_t length) {
    PacketType packet;
    for (std::size_t i = 0; i < length; i++) {
        packet[i] = i;
    }
    return packet;
}

void SBandTask::setConfiguration(uint16_t pllFrequency24, uint8_t pllChannelNumber24) {
    configFrequency.pllFrequency24 = pllFrequency24;
    configFrequency.pllChannelNumber24 = pllChannelNumber24;
    configFrequency.pllChannelMode24 = AT86RF215::PLLChannelMode::FineResolution2443;
    transceiver.config = configFrequency;
}

/*
* The frequency cannot be lower than 377000 as specified in section 6.3.2. The frequency range related
* to Fine Resolution Channel Scheme CNM.CM=1 is from 389.5MHz to 510MHz
*/
uint16_t SBandTask::calculatePllChannelFrequency24(uint32_t frequency) {
    uint32_t N = (frequency - 2366000) * 65536 / 26000;
    return N >> 8;
}

/*
* The frequency cannot be lower than 377000 as specified in section 6.3.2. The frequency range related
* to Fine Resolution Channel Scheme CNM.CM=1 is from 389.5MHz to 510MHz
*/
uint8_t SBandTask::calculatePllChannelNumber24(uint32_t frequency) {
    uint32_t N = (frequency - 2366000) * 65536 / 26000;
    return N & 0xFF;
}

void SBandTask::directModConfig(bool enable) {
    AT86RF215::Error err;
    transceiver.set_direct_modulation(AT86RF215::RF24, enable, err);
    uint8_t temp = (transceiver.spi_read_8(AT86RF215::RegisterAddress::BBC1_FSKDM, error)) & 0b11111110;
    // enable direct modulation and pre - emphasis filter
    transceiver.spi_write_8(AT86RF215::RegisterAddress::BBC1_FSKDM, temp | 0b00000011, error);
}

void SBandTask::modulationConfig(){
    AT86RF215::Error err;
    // BT = 1 , MIDXS = 1, MIDX = 1, MOR = B-FSK
    transceiver.spi_write_8(AT86RF215::RegisterAddress::BBC1_FSKC0, 86, err);
    directModConfig(true);
}



void SBandTask::execute() {
    setConfiguration(calculatePllChannelFrequency24(FrequencyS), calculatePllChannelNumber24(FrequencyS));

    transceiver.chip_reset(error);
//    transceiver.setup(error);
    transceiver.spi_write_8(AT86RF215::RegisterAddress::RF24_PADFE, 2 << 6, error);
//    transceiver.spi_write_8(AT86RF215::RegisterAddress::RF_CLKO, 0, error);

    uint16_t currentPacketLength = 2000;
    PacketType packet = createRandomPacket(currentPacketLength);
//    PacketType mes = {4, 34, 13, 67, 23, 78};
//    modulationConfig();
//    uint8_t message[MaxPacketLength] = {1, 2, 3, 4, 5, 6, 7};
    while (true) {
        transceiver.transmitBasebandPacketsTx(AT86RF215::RF24, packet.data(), currentPacketLength, error);
//        vTaskDelay(pdMS_TO_TICKS(500));
    }
}


