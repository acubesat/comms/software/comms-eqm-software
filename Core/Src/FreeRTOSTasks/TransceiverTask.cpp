#include "TransceiverTask.hpp"

AT86RF215::AT86RF215 TransceiverTask::transceiver = AT86RF215::AT86RF215(&hspi4, AT86RF215::AT86RF215Configuration());


TransceiverTask::PacketType TransceiverTask::createRandomPacket(uint16_t length) {
    PacketType packet;
    for (std::size_t i = 0; i < length; i++) {
        packet[i] = i;
    }
    return packet;
}

void TransceiverTask::setConfiguration(uint16_t pllFrequency09, uint8_t pllChannelNumber09) {
    configFrequency.pllFrequency09 = pllFrequency09;
    configFrequency.pllChannelNumber09 = pllChannelNumber09;
    configFrequency.pllChannelMode09 = AT86RF215::PLLChannelMode::FineResolution450;
    configFrequency.powerAmplifierRampTime09 = AT86RF215::PowerAmplifierRampTime::RF_PARAMP32U;
    configFrequency.transmitterCutOffFrequency09 = AT86RF215::TransmitterCutOffFrequency::RF_FLC80KHZ;
    transceiver.config = configFrequency;
}

/*
* The frequency cannot be lower than 377000 as specified in section 6.3.2. The frequency range related
* to Fine Resolution Channel Scheme CNM.CM=1 is from 389.5MHz to 510MHz
*/
uint16_t TransceiverTask::calculatePllChannelFrequency09(uint32_t frequency) {
    uint32_t N = (frequency - 377000) * 65536 / 6500;
    return N >> 8;
}

/*
* The frequency cannot be lower than 377000 as specified in section 6.3.2. The frequency range related
* to Fine Resolution Channel Scheme CNM.CM=1 is from 389.5MHz to 510MHz
*/
uint8_t TransceiverTask::calculatePllChannelNumber09(uint32_t frequency) {
    uint32_t N = (frequency - 377000) * 65536 / 6500;
    return N & 0xFF;
}

void  TransceiverTask::directModConfig(bool enable) {
    AT86RF215::Error err;
    transceiver.set_direct_modulation(AT86RF215::RF09, enable, err);
    uint8_t temp = (transceiver.spi_read_8(AT86RF215::RegisterAddress::BBC0_FSKDM, error)) & 0b11111110;
    // enable direct modulation and pre - emphasis filter
    transceiver.spi_write_8(AT86RF215::RegisterAddress::BBC0_FSKDM, temp | 0b00000011, error);
}

void TransceiverTask::modulationConfig(){
    AT86RF215::Error err;
    // BT = 1 , MIDXS = 1, MIDX = 1, MOR = B-FSK
    transceiver.spi_write_8(AT86RF215::RegisterAddress::BBC0_FSKC0, 86, err);
    directModConfig(true);
}


void TransceiverTask::execute() {
    setConfiguration(calculatePllChannelFrequency09(FrequencyUHF), calculatePllChannelNumber09(FrequencyUHF));
    transceiver.chip_reset(error);
    transceiver.spi_write_8(AT86RF215::RegisterAddress::RF09_PADFE, 2 << 6, error);
//    transceiver.setup(error);
//    while (true) {
//        auto read = transceiver.spi_read_8(0x0E, error);
////        LOG_DEBUG << "Version number:\t" << versionNumber;
//        LOG_DEBUG << "Reads:\t" << read;
//        vTaskDelay(pdMS_TO_TICKS(DelayMs));
//    }

    uint16_t currentPacketLength = 64;
    PacketType packet = createRandomPacket(currentPacketLength);

    modulationConfig();

    while (true) {
//        auto padfe = transceiver.spi_read_8(AT86RF215::RegisterAddress::RF09_PADFE, error);
//        LOG_DEBUG << "PADFE value: " << padfe;
//        auto pn = static_cast<uint8_t>(transceiver.get_part_number(error));
//        LOG_DEBUG << "Version Number:\t" << vn;
//        LOG_DEBUG << "Part Number:\t" << pn;
        transceiver.transmitBasebandPacketsTx(AT86RF215::RF09, packet.data(), currentPacketLength, error);
        vTaskDelay(pdMS_TO_TICKS(DelayMs));
    }
}
