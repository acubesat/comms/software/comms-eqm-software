#pragma once

#include "Task.hpp"
#include "main.h"
#include "at86rf215.hpp"
#include "at86rf215config.hpp"
#include "queue.h"
#include <etl/optional.h>

extern SPI_HandleTypeDef hspi4;

class SBandTask : public Task {
public:
    void execute();

    constexpr static uint16_t MaxPacketLength = 2000;
    using PacketType = etl::array<uint8_t, MaxPacketLength>;

    SBandTask() : Task("SBand signal transmission") {}

    void modulationConfig();
    void directModConfig(bool enable);

    /*
     * This function creates random packets until we have full functionality.
     */
    PacketType createRandomPacket(uint16_t length);

    /*
     * This function calculates the PllChannelFrequency value using the formula given in the datasheet
     * for Fine Resolution Channel Scheme CNM.CM=1 (section 6.3.2)
     */
    uint16_t calculatePllChannelFrequency24(uint32_t frequency);

    /*
     * This function calculates the PllChannelNumber value using the formula given in the datasheet
     * for Fine Resolution Channel Scheme CNM.CM=1 (section 6.3.2)
     */
    uint8_t calculatePllChannelNumber24(uint32_t frequency);

    void setConfiguration(uint16_t pllFrequency24, uint8_t pllChannelNumber24);

//    static inline AT86RF215::AT86RF215 transceiver = AT86RF215::AT86RF215(&hspi4, AT86RF215::AT86RF215Configuration());

    void createTask() {
        xTaskCreateStatic(vClassTask < SBandTask > , this->TaskName,
                          SBandTask::TaskStackDepth, this, tskIDLE_PRIORITY + 1,
                          this->taskStack, &(this->taskBuffer));
    }

private:
    AT86RF215::AT86RF215Configuration configFrequency;

    constexpr static uint16_t DelayMs = 10;
    constexpr static uint16_t TaskStackDepth = 2000;
    constexpr static uint32_t FrequencyUHF = 436500;
//    constexpr static uint32_t FrequencyS = 2483000;
    constexpr static uint32_t FrequencyS = 2425000;

    QueueHandle_t packetQueue;
    AT86RF215::Error error;
    StackType_t taskStack[TaskStackDepth];

};

inline etl::optional<SBandTask> sBandTask;

